import environ
from django.urls import reverse_lazy


BASE_DIR = environ.Path(__file__) - 3
ROOT_DIR = environ.Path(__file__) - 4

env = environ.Env()
env.read_env(ROOT_DIR(".env"))

DEBUG = env.bool('DEBUG', default=False)
TEMPLATE_DEBUG = DEBUG

DATABASES = {'default': env.db('DATABASE_URL')}

MEDIA_ROOT = ROOT_DIR('media')
MEDIA_URL = env.str('MEDIA_URL', default='/media/')

STATIC_ROOT = ROOT_DIR('collectstatic')
STATIC_URL = env.str('STATIC_URL', default='/static/')

STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
STATICFILES_DIRS = [
    BASE_DIR('static')
]
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

SECRET_KEY = env.str('SECRET_KEY', default='3x,bXJh94>]/S&+(H^#!6$h[{nQ#ZB4v95#F~PXE')

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'wsgi.application'

LOGIN_REDIRECT_URL = reverse_lazy('crud:statement-list')
LOGOUT_REDIRECT_URL = reverse_lazy('login')

CSRF_USE_SESSIONS = True

AUTH_USER_MODEL = 'crud.User'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

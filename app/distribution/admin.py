from django.contrib import admin

from distribution.models import Payment


# Register your models here.
@admin.register(Payment)
class PaymentModelAdmin(admin.ModelAdmin):
    ...

from django.db import models


class Payment(models.Model):
    amount = models.DecimalField('Сумма выплаты', max_digits=7, decimal_places=2)
    date = models.DateField('Дата выплаты')
    statement = models.OneToOneField('crud.statement', related_name='payment', verbose_name='Заявление', null=True,
                                     on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Выплата"
        verbose_name_plural = "Выплаты"

    def __str__(self):
        return f"{self.amount}р. от {self.date}"

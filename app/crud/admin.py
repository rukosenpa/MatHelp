from django.contrib import admin

from crud.models import StatementCategory, Statement, Student, Group


@admin.register(StatementCategory)
class StatementCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Statement)
class StatementAdmin(admin.ModelAdmin):
    pass


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    pass


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    pass

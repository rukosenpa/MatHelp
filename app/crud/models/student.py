from django.db import models
from django.urls import reverse

GENDERS = (
    ('male', 'Мужской'),
    ('female', 'Женский'),
)


class Student(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=50)
    last_name = models.CharField(verbose_name='Фамилия', max_length=50)
    middle_name = models.CharField(verbose_name='Отчество', max_length=50)

    gender = models.CharField(verbose_name='Пол', choices=GENDERS, max_length=6)

    student_card = models.IntegerField(verbose_name='Номер студенческого билета', unique=True)

    birth_date = models.DateField(verbose_name='Дата рождения')

    group = models.ForeignKey('crud.Group', verbose_name='Группа', related_name='students',
                              null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    def __str__(self):
        return ' '.join([self.last_name, self.first_name, self.middle_name])

    ############################
    # Template meta settings ###
    ############################

    update_header = 'Изменить данные студента'
    create_header = 'Добавить студента'
    list_header = 'Список студентов'

    success_message_create = 'Студент успешно создан'
    success_message_delete = 'Стдудент успешно удален'

    delete_modal_title = 'Удалить студента'

    @property
    def update_url(self):
        return reverse('crud:students-update', kwargs={'id': self.id})

    @property
    def delete_url(self):
        return reverse('crud:students-delete', kwargs={'id': self.id})

    @property
    def create_url(self):
        return reverse('crud:students-create')

    ############################

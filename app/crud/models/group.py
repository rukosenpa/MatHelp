from django.db import models

STUDYING_TYPES = (
    ('full-time', 'Очная'),
    ('part-time', 'Заочная'),
)

STUDYING_GRADES = (
    ('baccalaureate', 'Бакалавриат'),
    ('magistrate', 'Магистратура'),
)


class Group(models.Model):
    program_title = models.CharField(verbose_name='Название программы', max_length=50)

    studying_type = models.CharField(verbose_name='Тип обучения', choices=STUDYING_TYPES, max_length=10)
    studying_grade = models.CharField(verbose_name='Ступень обучения', choices=STUDYING_GRADES, max_length=13)

    alias = models.CharField(verbose_name='Сокращенное название', max_length=50, blank=True)

    admission_date = models.DateField(verbose_name='Год поступления')

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.alias

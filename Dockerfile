FROM python:3.8-slim

RUN set -ex \
    && DEPS=" \
    libpcre3 \
    mime-support \
    postgresql-client \
    build-essential \
    libpcre3-dev \
    libpq-dev \
    " \
    && seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{} \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

# Copy in your requirements file
ADD requirements.txt /requirements.txt

RUN pip install --no-cache-dir -r requirements.txt \
    && rm -rf /var/lib/apt/lists/*

# Copy your application code to the container (make sure you create a .dockerignore file if any large files or directories should be excluded)
RUN mkdir /app
WORKDIR /app
ADD app /app

# Call collectstatic (customize the following line with the minimal environment variables needed for manage.py to run):
RUN DATABASE_URL=''  python manage.py collectstatic --noinput -v 0
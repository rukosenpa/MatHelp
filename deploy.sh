#!/bin/sh

set -e

> .env
echo "VERSION=$2" >> .env
echo "CI_PROJECT_NAMESPACE=$CI_PROJECT_NAMESPACE" >> .env
echo "CI_PROJECT_NAME=$CI_PROJECT_NAME" >> .env
echo "CI_REGISTRY=$CI_REGISTRY" >> .env
echo "POSTGRES_USER=$DB_USER" >> .env
echo "POSTGRES_PASSWORD=$DB_PASSWORD" >> .env
echo "POSTGRES_DB=$DB_NAME" >> .env
echo "DATABASE_URL=postgres://$DB_USER:$DB_PASSWORD@db:5432/$DB_NAME" >> .env


echo "login docker registry"
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP "docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY"
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP "mkdir -p /data/$CI_PROJECT_NAME"


echo "copy docker-compose file"
sshpass -p $SERVER_PASSWORD scp $SSH_OPT ./docker-compose.yml root@$SERVER_IP:/data/$CI_PROJECT_NAME/docker-compose.yml;


echo "copy .env file"
sshpass -p $SERVER_PASSWORD scp $SSH_OPT ./.env root@$SERVER_IP:/data/$CI_PROJECT_NAME/


echo "pull images"
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP "cd /data/$CI_PROJECT_NAME/ && docker-compose pull"


echo "migrate..."
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP "cd /data/$CI_PROJECT_NAME/ && docker-compose run --rm server bash -c 'python manage.py migrate --noinput'"


echo "start services"
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP "cd /data/$CI_PROJECT_NAME/ && docker-compose up -d"

echo "remove none docker images"
sshpass -p $SERVER_PASSWORD ssh $SSH_OPT root@$SERVER_IP 'true || docker rmi -f $(docker images -f "dangling=true" -q)'
